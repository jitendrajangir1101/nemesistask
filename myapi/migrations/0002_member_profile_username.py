# Generated by Django 3.1.1 on 2021-04-22 20:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myapi', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='member_profile',
            name='username',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
