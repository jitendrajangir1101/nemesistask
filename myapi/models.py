from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser ,UserManager ,BaseUserManager

#Here i am extending User model.

class member_profile(AbstractUser): 
    username =  None
    firstName=models.CharField(max_length=255)
    lastName=models.CharField(max_length=255)
    email = models.EmailField( unique = True ,null=True ,blank=True) 
    address = models.TextField(null=True ,blank=True)
    phoneNumber = models.CharField(max_length=10 ,unique=True)
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phoneNumber', 'first_name','last_name','address'] 
    def __str__(self): 
       return "{}".format(self.email)