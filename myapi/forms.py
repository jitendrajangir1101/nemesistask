from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm ,AuthenticationForm ,UsernameField ,PasswordChangeForm ,PasswordResetForm,SetPasswordForm
from django.utils.translation import gettext, gettext_lazy as _
from django.contrib.auth import  password_validation
from .models import member_profile

#Form for member registration
class MemberCreationForm(UserCreationForm):
    class Meta:
        model = member_profile
        fields = ( 'firstName', 'lastName' ,'email','phoneNumber','address')
        labels = {'firstName':'First Name' ,'lastName':'Last Name','address':'Address','email':'Email','phoneNumber':'Phone Number'}
        widgets = {
            'firstName' :forms.TextInput(attrs={'class':'form-control'}),
            'lastName' :forms.TextInput(attrs={'class':'form-control'}),
            'email' :forms.EmailInput(attrs={'class':'form-control'}),
            'phoneNumber' :forms.TextInput(attrs={'class':'form-control'}),
            'address' :forms.TextInput(attrs={'class':'form-control'}),
                    }
    def __init__(self, *args, **kwargs):
        super(MemberCreationForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget = forms.PasswordInput(attrs={'class': 'form-control'})
        self.fields['password2'].widget = forms.PasswordInput(attrs={'class': 'form-control'})

#Form for login a user.
class loginForm(AuthenticationForm):
    username = UsernameField(label='Email' ,widget=forms.TextInput(attrs={'autofocus' : True  ,'class' :'form-control'}))
    password=forms.CharField(label=_("Password"), strip=False , widget=forms.PasswordInput(attrs={'autocomplete':'current-password' , 'class':'form-control'}))


#Form for updating an Existing user .
class MemberUpdateForm(forms.ModelForm):
    class Meta:
        model = member_profile
        fields = ( 'firstName', 'lastName' ,'email','phoneNumber','address')
        labels = {'firstName':'First Name' ,'lastName':'Last Name','address':'Address','email':'Email','phoneNumber':'Phone Number'}
        widgets = {
            'firstName' :forms.TextInput(attrs={'class':'form-control'}),
            'lastName' :forms.TextInput(attrs={'class':'form-control'}),
            'email' :forms.EmailInput(attrs={'class':'form-control'}),
            'phoneNumber' :forms.TextInput(attrs={'class':'form-control'}),
            'address' :forms.TextInput(attrs={'class':'form-control'}),
                    }