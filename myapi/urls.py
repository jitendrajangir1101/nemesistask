
from django.urls import path, include
from rest_framework import permissions

from django.urls import path
from . import views
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
#from .forms importMyPasswordChangeForm, MyPasswordResetForm, MySetPasswordForm
from django.conf.urls import url
urlpatterns = [
#Web Api urls here.
        path('', views.MemberRegistrationView.as_view(),name='memberregistration'),
        path('login/' , views.user_login , name='login'),
        path('profile/' , views.profile , name='profile'),
        path('logout/' , views.user_logout , name='logout'),
        path('update/<id>',views.updateInfo ,name ='updateInfo'),
 # Rest Api urls here...............       
        url('api/signup', views.UserRegistrationView.as_view()),
        url('api/signin', views.userLogin),
        url('api/profile', views.UserProfileView.as_view()),


]