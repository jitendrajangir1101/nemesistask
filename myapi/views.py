from django.shortcuts import render
from django.views import View
from .forms import MemberCreationForm,loginForm,MemberUpdateForm
from django.contrib.auth import authenticate , login , logout ,update_session_auth_hash
from django.contrib import messages
from django.http import HttpResponseRedirect
from .models import member_profile
from django.shortcuts import render, get_object_or_404, redirect
from rest_framework import status
from rest_framework.generics import CreateAPIView,RetrieveAPIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from .serializers import UserRegistrationSerializer,UserLoginSerializer
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

# View for registering an user.
class MemberRegistrationView(View):
    def get(self, request):
        form = MemberCreationForm()
        return render(request, 'myapi/memberregistration.html', {'form': form})

    def post(self, request):
        form = MemberCreationForm(request.POST)
        

        if form.is_valid():
            form.save()
            messages.success(
                request, 'congratulations ! you are now registered with us')
            return HttpResponseRedirect('/')

        return render(request, 'myapi/memberregistration.html', {'form': form})

#Function for logout an user.
def user_logout(request):
    logout(request)
    messages.success(request, 'Logged Out Successfully')
    return HttpResponseRedirect("/")

#Function for login an user.
def user_login(request):
    if request.method == 'POST':
        form = loginForm(request=request, data=request.POST)
        if form.is_valid():
            uname = form.cleaned_data['username']
            upass = form.cleaned_data['password']
            user = authenticate(username=uname, password=upass)
            if user is not None:
               
                login(request, user)
                messages.success(request, 'logged in ')

                return HttpResponseRedirect('/profile/')
    else:
        form = loginForm()
    return render(request, 'myapi/login.html', {'form': form})

#Function for displayin user profile.
def profile(request):
    return render(request , 'myapi/profile.html')

#Function for updating information of an existing user.
def updateInfo(request,id):
    obj = get_object_or_404(member_profile, id=id)

    form = MemberUpdateForm(request.POST or None, instance=obj)
    
    context = {'form': form}
    if form.is_valid():
        obj = form.save(commit=False)
        obj.save()
        messages.success(request, "You successfully updated the post")
        return redirect('/profile/')

    else:
        context = {'form':form , 'error': 'The form was not updated successfully'}
        return render(request, 'myapi/updateInfo.html', context)


#Rest api views starts here.
#Rest api view for user registration.
class UserRegistrationView(CreateAPIView):

    serializer_class = UserRegistrationSerializer
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        status_code = status.HTTP_201_CREATED
        response = {
            'success' : 'True',
            'status code' : status_code,
            'message': 'User registered  successfully',
            }
        
        return Response(response, status=status_code)

#Rest api function login an user and jwt token generation.
@csrf_exempt
@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def userLogin(request):
    authentication_classes = []  # disables authentication
    permission_classes = []
    serializer_class = UserLoginSerializer
    serializer = serializer_class(data=request.data)
    serializer.is_valid(raise_exception=True)
    email = request.data.get("email")
    password = request.data.get("password")
    if email is None or password is None:
        return Response({'error': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)
    user = authenticate(email=email, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    
    return Response({'token':  serializer.data['token'], "user_id": user.id, "user_id": user.id, "msg": " u r logged in "},
                    status=HTTP_200_OK)

#Rest api view for accessing profile of an authorized user using jwt toekn.
class UserProfileView(RetrieveAPIView):

    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request):
        try:
            user_profile = member_profile.objects.get(user=request.user)
            status_code = status.HTTP_200_OK
            response = {
                'success': 'true',
                'status code': status_code,
                'message': 'User profile fetched successfully',
                'data': [{
                    'first_name': user_profile.firstName,
                    'last_name': user_profile.lastName,
                    'Phone_number': user_profile.phoneNumber,
                    'Email': user_profile.address,
                    'Address': user_profile.email,
                    }]
                }

        except Exception as e:
            status_code = status.HTTP_400_BAD_REQUEST
            response = {
                'success': 'false',
                'status code': status.HTTP_400_BAD_REQUEST,
                'message': 'User does not exists',
                'error': str(e)
                }
        return Response(response, status=status_code)
